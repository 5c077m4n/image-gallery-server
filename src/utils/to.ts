/**
 * @function to - runs the function as a Promise resolve to catch errors (for await)
 * @param fn is the function to run
 * @param args is the input arguments
 * @returns a Promise of the array [error, data]
 */
export default async function to(fn: Function, ...args: any[]): Promise<any[]> {
	return Promise.resolve(fn(...args))
		.then((data: any) => [undefined, data])
		.catch((error: Error) => [error, undefined]);
}
