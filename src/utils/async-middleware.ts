import {Request, Response, NextFunction} from 'express';


/**
 * @function wraps a function and catches its errors
 * @param fn is the input function to wrap
 * @returns a function conaining a Promise with a built-in error handler attached
 */
export default function asyncMiddleware(fn: Function): any {
	return (req: Request, res: Response, next: NextFunction) => {
		Promise.resolve(fn(req, res, next))
		.catch(next);
	};
}
