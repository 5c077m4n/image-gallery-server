import {Response} from 'express';


export class HttpError extends Error {
	public status: number;
	public location?: string;
	public timestamp: Date;
	constructor(status: number = 500, message?: string, location?: string) {
		status = (status >= 100 && status < 600)? status : 500;
		if(!message) {
			if(status === 400) message = 'Bad Request.';
			if(status === 401) message = 'Unauthorized.';
			if(status === 403) message = 'Forbidden.';
			if(status === 404) message = 'Not Found.';
			if(status === 500) message = 'Internal Server Error.';
		}
		super(message);
		// Object.setPrototypeOf(this, HttpError.prototype);
		this.status = status;
		this.location = location;
		this.timestamp = new Date();
	}

	public throwError() {
		throw this;
	}
	public returnAsResponse(res: Response): Response {
		return res.status(this.status).json({
			error: {
				status: this.status,
				message: this.message,
				location: this.location
			}
		});
	}
	public toString() {
		if(!this.location) return `status: ${this.status}, message: ${this.message}`;
		else return `status: ${this.status}, message: ${this.message} @ ${this.location}`;
	}
}

export interface HttpError {
	status: number;
	message: string;
	location?: string;
	timestamp: Date;
}
