import http from 'http';
import process from 'process';
import util from 'util';

import express, {Request, Response, NextFunction} from 'express';
import bodyParser from 'body-parser';
import compress from 'compression';
import cloudinary from 'cloudinary';
import cors from 'cors';
import helmet from 'helmet';
import chokidar from 'chokidar';

import RouteFunctions from './routes/routerFunctions';
import asyncMiddleware from '../utils/async-middleware';
import Picture from '../models/picture.interface';
import to from '../utils/to';


const log: Function = console.log.bind(console);
const cloud = Promise.promisifyAll(cloudinary.v2.api);
let isScanComplete: boolean = false;

export default class Server {
	private readonly PORT: number;
	private readonly app: express.Application;
	private watcher: any;
	constructor() {
		this.PORT = (process.env.PORT)? +process.env.PORT : 3000;
		this.app = express();
		this.configServer();
		this.configCloud();
		RouteFunctions.emptyImageFolder();
		this.addWatcher();
	}

	private configServer() {
		this.app.use(compress({
			filter: (req: Request, res: Response): any =>
				(req.headers['x-no-compression'])? undefined : compress.filter(req, res),
			level: 6
		}));
		this.app.options('*', cors());
		this.app.use(cors());
		this.app.use(helmet());
		this.app.use(bodyParser.urlencoded({extended: false}));

		/** routes */
		this.app.route('/')
		.all(asyncMiddleware(RouteFunctions.test));

		this.app.route('/photos')
		.get(asyncMiddleware(RouteFunctions.getCloudData))
		.post(asyncMiddleware(RouteFunctions.upload));

		this.app.route('/photos/:id')
		.get(asyncMiddleware(RouteFunctions.getPictureById))
		.delete(asyncMiddleware(RouteFunctions.deletePictureById));

		/** 404 & error handler - to put at the end! */
		this.app.use(asyncMiddleware(RouteFunctions.handle404));
		this.app.use(asyncMiddleware(RouteFunctions.errorHandler));

		/** Start server */
		http
		.createServer(this.app)
		.listen(this.PORT, () => log(
			`[server] express is now running on port ${this.PORT} in ${this.app.get('env')} mode.`
		))
		.on('error', err => console.error(`[server] connection error: ${err}`));
	}
	private configCloud(): void {
		cloudinary.config({
			cloud_name: 'a-5c077m4n',
			api_key: '671718659951117',
			api_secret: 'XjNoM1x105M2CGDMjJls4zLh37g'
		});
	}
	private addWatcher(): void {
		this.watcher = chokidar.watch('./public/images/', {
			ignored: /[\/\\]\./,
			persistent: true
		});
		this.watcher
		.on('ready', (): void => {
			log('[watcher] initial scan complete. Ready for changes.');
			isScanComplete = true;
		})
		.on('add', async (path: string): Promise<void> => {
			if(isScanComplete) {
				log(`[watcher] file ${path} has been added.`);
				await cloud.uploader.uploadAsync(
					path,
					{folder: '/gallery', use_filename: true, tags: ['untagged']}
				)
				.then((result: any) => log(
					`[cloudinary] result of upload: ${result.secure_url}, insecure url: ${result.url}.`
				))
				.catch((error: Error) => {
					log(`[cloudinary] an error has occured: ${error.message}.`);
					Promise.reject(error);
				});
				RouteFunctions.emptyImageFolder();
			}
		})
		.on('addDir', (path: string): void => log(`[watcher] directory ${path} has been added.`))
		.on('change', (path: string): void => log(`[watcher] File ${path} has been changed.`))
		.on('unlink', (path: string): void => log(`[watcher] file ${path} has been removed.`))
		.on('error', (error: Error): void => log(`[watcher] an error has occurred: ${error}.`));
	}
}
