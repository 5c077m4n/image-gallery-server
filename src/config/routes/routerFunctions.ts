import fs from 'fs';
import path from 'path';

import {Request, Response, NextFunction} from 'express';
import formidable from 'formidable';
import cloudinary from 'cloudinary';

import {HttpError} from '../../utils/http-error.class';
import to from '../../utils/to';


const log: Function = console.log.bind(console);
const cloud = Promise.promisifyAll(cloudinary.api);
const fsReaddirAsync: Function = Promise.promisify(fs.readdir);
const fsUnlinkAsync: Function = Promise.promisify(fs.unlink);
const isBusy: Function = (funcName: string) => log(`[${funcName}] working, just a second...`);

export default class RouteFunctions {
	constructor() {}

	public static async test(req: Request, res: Response, next: NextFunction): Promise<Response> {
		return res.status(200).json({response: "Great Success!"});
	}
	public static async getCloudData(req: Request, res: Response, next: NextFunction): Promise<Response> {
		isBusy('getCloudData');
		const response = await cloud.resourcesAsync({type: 'upload'})
			.then((result: any) => result.resources)
			.catch(next);
		return res.status(200).json({response});
	}
	public static async getPictureById(req: Request, res: Response, next: NextFunction): Promise<Response> {
		isBusy('getPictureById');
		const response = await cloud.resources_by_idsAsync([req.params.id])
			.then((result: any) => result.resources)
			.catch(next);
		return res.status(200).json({response});
	}
	public static async getPicturesByTag(req: Request, res: Response, next: NextFunction): Promise<Response> {
		isBusy('getPicturesByTag');
		const response = await cloud.resources_by_tagAsync(req.params.tag)
			.then((result: any) => result.resources)
			.catch(next);
		return res.status(200).json({response});
	}
	public static async upload(req: Request, res: Response, next: NextFunction): Promise<Response> {
		const form = new formidable.IncomingForm();
		Promise.promisifyAll(form);

		form.type = 'multipart';
		form.encoding = 'utf-8';
		form.uploadDir = 'public/images';
		form.keepExtensions = true;
		form.maxFieldsSize = 2 * 1024 * 1024;
		form.maxFileSize = 10 * 1024 * 1024;
		form.on('progress', (bytesReceived: number, bytesExpected: number) =>
			log(`[formidable] ${bytesReceived}/${bytesExpected} has arrived (${bytesReceived/bytesExpected*100}%).`)
		);

		const response = await form.parseAsync(req)
			.then((files: any) => files)
			.catch(Promise.reject);
		return res.status(200).json({response});
	}
	public static async deletePictureById(req: Request, res: Response, next: NextFunction): Promise<Response> {
		isBusy('deletePictureById');
		const response = await cloud.delete_resourcesAsync([req.params.id])
			.then((result: any) => result)
			.catch(next);
		return res.status(200).json({response});
	}
	public static async deletePicturesByTag(req: Request, res: Response, next: NextFunction): Promise<Response> {
		isBusy('deletePicturesByTag');
		const response = await cloud.delete_resources_by_tagAsync(
			[req.params.tag],
			(error: Error, result: object) => (error)? next(error) : result
		)
		.then((res: any) => res)
		.catch((err: Error) => Promise.reject(err));
		return res.status(200).json({response});
	}

	public static emptyImageFolder(): void {
		fsReaddirAsync('./public/images')
		.map((file: string) => {
			fsUnlinkAsync(path.join('./public/images', file))
			.then(() => log(`[fs] the file ${file} has been deleted.`))
			.catch(Promise.reject);
		})
		.catch(console.error);
	}

	public static handle404(req: Request, res: Response, next: NextFunction): void {
		isBusy('handle404');
		return next(new HttpError(404, 'The requested page cannot be found.'));
	}
	public static errorHandler(error: HttpError, req: Request, res: Response, next: NextFunction): Response {
		isBusy('errorHandler');

		if(!error.location) console.error(`[server] error: "${error.status} - ${error.message}"`);
		else console.error(`[server - ${error.location}] error: "${error.status} - ${error.message}"`);

		return res.status((error.status >= 100 && error.status < 600)? error.status : 500).json({
			error: {
				status: error.status,
				message: error.message,
				location: error.location
			}
		});
	}
}
