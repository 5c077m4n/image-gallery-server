export default interface Picture {
	path?: string;
	name?: string;
	tag?: string;
}
