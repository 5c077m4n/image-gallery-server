"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class HttpError extends Error {
    constructor(status = 500, message, location) {
        status = (status >= 100 && status < 600) ? status : 500;
        if (!message) {
            if (status === 400)
                message = 'Bad Request.';
            if (status === 401)
                message = 'Unauthorized.';
            if (status === 403)
                message = 'Forbidden.';
            if (status === 404)
                message = 'Not Found.';
            if (status === 500)
                message = 'Internal Server Error.';
        }
        super(message);
        // Object.setPrototypeOf(this, HttpError.prototype);
        this.status = status;
        this.location = location;
        this.timestamp = new Date();
    }
    throwError() {
        throw this;
    }
    returnAsResponse(res) {
        return res.status(this.status).json({
            error: {
                status: this.status,
                message: this.message,
                location: this.location
            }
        });
    }
    toString() {
        if (!this.location)
            return `status: ${this.status}, message: ${this.message}`;
        else
            return `status: ${this.status}, message: ${this.message} @ ${this.location}`;
    }
}
exports.HttpError = HttpError;
