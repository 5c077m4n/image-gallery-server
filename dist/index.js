"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bluebird_1 = __importDefault(require("bluebird"));
global.Promise = bluebird_1.default;
const server_1 = __importDefault(require("./config/server"));
const app = new server_1.default();
