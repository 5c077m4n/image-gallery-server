"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const process_1 = __importDefault(require("process"));
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const compression_1 = __importDefault(require("compression"));
const cloudinary_1 = __importDefault(require("cloudinary"));
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet"));
const chokidar_1 = __importDefault(require("chokidar"));
const routerFunctions_1 = __importDefault(require("./routes/routerFunctions"));
const async_middleware_1 = __importDefault(require("../utils/async-middleware"));
const log = console.log.bind(console);
const cloud = Promise.promisifyAll(cloudinary_1.default.v2.api);
let isScanComplete = false;
class Server {
    constructor() {
        this.PORT = (process_1.default.env.PORT) ? +process_1.default.env.PORT : 3000;
        this.app = express_1.default();
        this.configServer();
        this.configCloud();
        routerFunctions_1.default.emptyImageFolder();
        this.addWatcher();
    }
    configServer() {
        this.app.use(compression_1.default({
            filter: (req, res) => (req.headers['x-no-compression']) ? undefined : compression_1.default.filter(req, res),
            level: 6
        }));
        this.app.options('*', cors_1.default());
        this.app.use(cors_1.default());
        this.app.use(helmet_1.default());
        this.app.use(body_parser_1.default.urlencoded({ extended: false }));
        /** routes */
        this.app.route('/')
            .all(async_middleware_1.default(routerFunctions_1.default.test));
        this.app.route('/photos')
            .get(async_middleware_1.default(routerFunctions_1.default.getCloudData))
            .post(async_middleware_1.default(routerFunctions_1.default.upload));
        this.app.route('/photos/:id')
            .get(async_middleware_1.default(routerFunctions_1.default.getPictureById))
            .delete(async_middleware_1.default(routerFunctions_1.default.deletePictureById));
        /** 404 & error handler - to put at the end! */
        this.app.use(async_middleware_1.default(routerFunctions_1.default.handle404));
        this.app.use(async_middleware_1.default(routerFunctions_1.default.errorHandler));
        /** Start server */
        http_1.default
            .createServer(this.app)
            .listen(this.PORT, () => log(`[server] express is now running on port ${this.PORT} in ${this.app.get('env')} mode.`))
            .on('error', err => console.error(`[server] connection error: ${err}`));
    }
    configCloud() {
        cloudinary_1.default.config({
            cloud_name: 'a-5c077m4n',
            api_key: '671718659951117',
            api_secret: 'XjNoM1x105M2CGDMjJls4zLh37g'
        });
    }
    addWatcher() {
        this.watcher = chokidar_1.default.watch('./public/images/', {
            ignored: /[\/\\]\./,
            persistent: true
        });
        this.watcher
            .on('ready', () => {
            log('[watcher] initial scan complete. Ready for changes.');
            isScanComplete = true;
        })
            .on('add', (path) => __awaiter(this, void 0, void 0, function* () {
            if (isScanComplete) {
                log(`[watcher] file ${path} has been added.`);
                yield cloud.uploader.uploadAsync(path, { folder: '/gallery', use_filename: true, tags: ['untagged'] })
                    .then((result) => log(`[cloudinary] result of upload: ${result.secure_url}, insecure url: ${result.url}.`))
                    .catch((error) => {
                    log(`[cloudinary] an error has occured: ${error.message}.`);
                    Promise.reject(error);
                });
                routerFunctions_1.default.emptyImageFolder();
            }
        }))
            .on('addDir', (path) => log(`[watcher] directory ${path} has been added.`))
            .on('change', (path) => log(`[watcher] File ${path} has been changed.`))
            .on('unlink', (path) => log(`[watcher] file ${path} has been removed.`))
            .on('error', (error) => log(`[watcher] an error has occurred: ${error}.`));
    }
}
exports.default = Server;
