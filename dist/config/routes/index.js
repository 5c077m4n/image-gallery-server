"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = __importDefault(require("util"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const formidable_1 = __importDefault(require("formidable"));
const cloudinary_1 = require("cloudinary");
const http_error_class_1 = require("../../utils/http-error.class");
const log = console.log.bind(console);
const readdirPromise = util_1.default.promisify(fs_1.default.readdir);
const unlinkPromise = util_1.default.promisify(fs_1.default.unlink);
class RouteFunctions {
    constructor() { }
    static test(req, res, next) {
        return res.status(200).json({ response: "Great Success!" });
    }
    static getCloudData(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            return res.status(200).json({ response: yield cloudinary_1.v2.api.resources({ type: 'upload' }, (error, result) => (error) ? Promise.reject(error) : result.resources)
                    .catch(Promise.reject)
            });
        });
    }
    static getPictureById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            return res.status(200).json({ response: yield cloudinary_1.v2.api.resources_by_ids([req.params.id], (error, result) => (error) ? Promise.reject(error) : result.resources)
                    .catch(Promise.reject)
            });
        });
    }
    static getPicturesByTag(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            return res.status(200).json({ response: yield cloudinary_1.v2.api.resources_by_tag(req.params.tag, (error, result) => (error) ? Promise.reject(error) : result.resources)
                    .catch(Promise.reject)
            });
        });
    }
    static upload(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const form = new formidable_1.default.IncomingForm();
            const parseForm = util_1.default.promisify(form.parse);
            form.type = 'multipart';
            form.encoding = 'utf-8';
            form.uploadDir = 'public/images';
            form.keepExtensions = true;
            form.maxFieldsSize = 2 * 1024 * 1024;
            form.maxFileSize = 10 * 1024 * 1024;
            form.on('progress', (bytesReceived, bytesExpected) => log(`[formidable] ${bytesReceived}/${bytesExpected} has arrived (${bytesReceived / bytesExpected * 100}%).`));
            const response = yield parseForm(req)
                .then((err, fields, files) => (err) ? Promise.reject(err) : { fields, files })
                .catch(Promise.reject);
            return res.status(200).json({ response });
        });
    }
    static deletePictureById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            return res.status(200).json({ response: yield cloudinary_1.v2.api.delete_resources([req.params.id], (error, result) => (error) ? Promise.reject(error) : result)
                    .catch(Promise.reject)
            });
        });
    }
    static deletePicturesByTag(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            return res.status(200).json({ response: yield cloudinary_1.v2.api.delete_resources_by_tag([req.params.tag], (error, result) => (error) ? Promise.reject(error) : result)
                    .catch(Promise.reject)
            });
        });
    }
    static emptyImageFolder() {
        return __awaiter(this, void 0, void 0, function* () {
            readdirPromise('./public/images')
                .then((files) => {
                files.forEach(file => {
                    unlinkPromise(path_1.default.join('./public/images', file))
                        .then(() => log(`[fs] the file ${file} has been deleted.`))
                        .catch(Promise.reject);
                });
            })
                .catch(Promise.reject);
        });
    }
    static handle404(req, res, next) {
        next(new http_error_class_1.HttpError(404, 'The requested page cannot be found.'));
    }
    static errorHandler(error, req, res, next) {
        console.error(`[server] error: "${error.status} - ${error.message}"`);
        return res.status((error.status >= 100 && error.status < 600) ? error.status : 500).json({
            error: {
                status: error.status,
                message: error.message
            }
        });
    }
}
exports.default = RouteFunctions;
